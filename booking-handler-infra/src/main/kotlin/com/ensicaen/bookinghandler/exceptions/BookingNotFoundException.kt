package com.ensicaen.bookinghandler.exceptions

class BookingNotFoundException(message: String) : RuntimeException(message)