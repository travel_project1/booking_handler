package com.ensicaen.bookinghandler.rentalhandler

import com.ensicaen.bookinghandler.authentication.getCurrentUserToken
import com.ensicaen.openapi.bookinghandler.api.DateRangeDto
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@FeignClient(name = "rental-handler", url = "\${rental-handler-url}")
interface RentalHandlerService {

    @PutMapping(
        path = ["/api/v1/rentals/{rentalId}/availabilities"],
        consumes = [APPLICATION_JSON_VALUE],
    )
    fun updateAvailabilities(
        @PathVariable("rentalId") rentalId: Int,
        @RequestBody unavailability: DateRangeDto,
        @RequestHeader(value = "Authorization") authorizationHeader: String = "Bearer ${getCurrentUserToken()}"
    ): ResponseEntity<Unit>
}