package com.ensicaen.bookinghandler.authentication

import com.ensicaen.bookinghandler.authentication.models.Credentials
import com.ensicaen.bookinghandler.authentication.models.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.FirebaseToken
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

private val LOGGER: Logger = LoggerFactory.getLogger(SecurityFilter::class.java)

@Component
class SecurityFilter : OncePerRequestFilter() {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain,
    ) {
        verifyToken(request)
        filterChain.doFilter(request, response)
    }

    private fun verifyToken(request: HttpServletRequest) {
        try {
            val bearerToken = getBearerToken(request)
            val firebaseToken = verifyIdToken(bearerToken)
            if (firebaseToken != null) {
                setUserInSecurityContext(
                    firebaseToken,
                    bearerToken,
                    request,
                )
            }
        } catch (e: FirebaseAuthException) {
            LOGGER.error("Firebase Exception:: ", e)
        }
    }

    private fun setUserInSecurityContext(
        firebaseToken: FirebaseToken,
        bearerToken: String,
        request: HttpServletRequest,
    ) {
        SecurityContextHolder.getContext().authentication =
            UsernamePasswordAuthenticationToken(
                firebaseTokenToUser(firebaseToken),
                Credentials(firebaseToken, bearerToken),
                null,
            ).apply { details = WebAuthenticationDetailsSource().buildDetails(request) }

    }

    private fun verifyIdToken(bearerToken: String): FirebaseToken? =
        FirebaseAuth.getInstance().verifyIdToken(bearerToken)

    private fun firebaseTokenToUser(firebaseToken: FirebaseToken) =
        User(
            uid = firebaseToken.uid,
            name = firebaseToken.name ?: firebaseToken.email.split("@")[0],
            email = firebaseToken.email,
        )

    private fun getBearerToken(request: HttpServletRequest) = request.getHeader("Authorization").substring(7)
}