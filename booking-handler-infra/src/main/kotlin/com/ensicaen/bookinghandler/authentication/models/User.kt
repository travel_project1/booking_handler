package com.ensicaen.bookinghandler.authentication.models

data class User(
    val uid: String,
    val name: String,
    val email: String,
)