package com.ensicaen.bookinghandler.authentication

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.util.ResourceUtils


@Configuration
class FirebaseConfiguration(private val authenticationProperties: AuthenticationProperties) {

    @Primary
    @Bean
    fun initializeFirebase() {
        val credentialsStream = ResourceUtils.getFile(authenticationProperties.credentialsPath).inputStream()
        val options =
            FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(credentialsStream))
                .build()
        FirebaseApp.initializeApp(options)
    }
}