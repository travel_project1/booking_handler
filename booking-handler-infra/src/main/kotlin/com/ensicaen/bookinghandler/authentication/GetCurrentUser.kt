package com.ensicaen.bookinghandler.authentication

import com.ensicaen.bookinghandler.authentication.models.Credentials
import com.ensicaen.bookinghandler.authentication.models.User
import org.springframework.security.core.context.SecurityContextHolder

fun getCurrentUser(): User = SecurityContextHolder.getContext().authentication.principal as User
fun getCurrentUserToken(): String =
    (SecurityContextHolder.getContext().authentication.credentials as Credentials).idToken