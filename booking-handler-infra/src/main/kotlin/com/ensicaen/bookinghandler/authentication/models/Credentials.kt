package com.ensicaen.bookinghandler.authentication.models

import com.google.firebase.auth.FirebaseToken

data class Credentials(
    val decodedToken: FirebaseToken,
    val idToken: String,
)