package com.ensicaen.bookinghandler

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

@EnableFeignClients
@SpringBootApplication
class BookingHandlerApplication

fun main(args: Array<String>) {
    runApplication<BookingHandlerApplication>(*args)
}
