package com.ensicaen.bookinghandler.configuration

import com.ensicaen.bookinghandler.BookingRegistryAbi
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.web3j.crypto.Credentials
import org.web3j.protocol.Web3j
import org.web3j.protocol.http.HttpService
import org.web3j.tx.gas.DefaultGasProvider

@Configuration
class BlockchainConfiguration(
    @Value("\${blockchain.url}") private val url: String,
    @Value("\${blockchain.contract-address}") private val contractAddress: String,
    @Value("\${blockchain.wallet-private-key}") private val walletPrivateKey: String,
) {

    @Bean
    fun createBookingRegistryBean(): BookingRegistryAbi =
        BookingRegistryAbi.load(
            contractAddress,
            Web3j.build(HttpService(url)),
            Credentials.create(walletPrivateKey),
            DefaultGasProvider()
        )

}