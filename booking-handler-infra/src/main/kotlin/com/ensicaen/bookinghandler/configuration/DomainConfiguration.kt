package com.ensicaen.bookinghandler.configuration

import com.ensicaen.bookinghandler.domain.BookingApiImpl
import com.ensicaen.bookinghandler.spi.BookingSpi
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class DomainConfiguration {

    @Bean
    fun createBookingApiBean(bookingSpi: BookingSpi) = BookingApiImpl(bookingSpi)
}