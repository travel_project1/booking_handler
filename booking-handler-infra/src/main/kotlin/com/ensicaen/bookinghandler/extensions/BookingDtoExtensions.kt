package com.ensicaen.bookinghandler.extensions

import com.ensicaen.bookinghandler.models.Booking
import com.ensicaen.openapi.bookinghandler.api.BookingDto

fun BookingDto.toDomain(bookerUid: String) = Booking(rentalId, bookerUid, begin, end)