package com.ensicaen.bookinghandler.extensions

import java.math.BigInteger
import java.time.OffsetDateTime

fun OffsetDateTime.toTimeStamp(): BigInteger = this.toInstant().epochSecond.toBigInteger()