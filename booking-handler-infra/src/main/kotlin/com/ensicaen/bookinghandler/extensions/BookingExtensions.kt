package com.ensicaen.bookinghandler.extensions

import com.ensicaen.bookinghandler.booking.BookingEntity
import com.ensicaen.bookinghandler.models.Booking
import com.ensicaen.openapi.bookinghandler.api.BookingDto

fun Booking.toEntity() = BookingEntity(rentalId, bookerUid, begin, end)

fun Booking.toDto() = BookingDto(rentalId, begin, end, id)