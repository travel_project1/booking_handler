package com.ensicaen.bookinghandler.booking

import com.ensicaen.bookinghandler.models.Booking
import com.sun.istack.NotNull
import java.time.OffsetDateTime
import javax.persistence.*
import javax.persistence.GenerationType.AUTO

@Entity
@Table(name = "Booking")
class BookingEntity(
    @Column @NotNull val rentalId: Int,
    @Column @NotNull val bookerUid: String,
    @Column(name = "begin_date") @NotNull val begin: OffsetDateTime,
    @Column(name = "end_date") @NotNull val end: OffsetDateTime,
) {
    @Id
    @GeneratedValue(strategy = AUTO)
    var id: Long? = null

    fun toDomain() = Booking(rentalId, bookerUid, begin, end, id?.toInt())
}