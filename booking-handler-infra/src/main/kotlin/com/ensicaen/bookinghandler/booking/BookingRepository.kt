package com.ensicaen.bookinghandler.booking

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.time.OffsetDateTime

@Repository
interface BookingRepository : JpaRepository<BookingEntity, Long> {

    fun existsByIdAndBeginAndEnd(
        id: Long,
        begin: OffsetDateTime,
        end: OffsetDateTime,
    ): Boolean

    fun findAllByBookerUid(bookerUid: String): List<BookingEntity>
}