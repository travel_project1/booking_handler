package com.ensicaen.bookinghandler.booking

import com.ensicaen.bookinghandler.BookingRegistryAbi
import com.ensicaen.bookinghandler.exceptions.BookingNotFoundException
import com.ensicaen.bookinghandler.extensions.toEntity
import com.ensicaen.bookinghandler.extensions.toTimeStamp
import com.ensicaen.bookinghandler.models.Booking
import com.ensicaen.bookinghandler.rentalhandler.RentalHandlerService
import com.ensicaen.bookinghandler.spi.BookingSpi
import com.ensicaen.openapi.bookinghandler.api.DateRangeDto
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class BookingSpiImpl(
    private val bookingRepository: BookingRepository,
    private val bookingRegistryAbi: BookingRegistryAbi,
    private val rentalHandlerService: RentalHandlerService,
) : BookingSpi {

    override fun create(booking: Booking) {
        val bookingEntity = bookingRepository.save(booking.toEntity())
        val transactionReceipt =
            bookingRegistryAbi
                .addBooking(
                    bookingEntity.id?.toBigInteger() ?: throw RuntimeException("booking id should not be null"),
                    bookingEntity.rentalId.toBigInteger(),
                    bookingEntity.bookerUid,
                    bookingEntity.begin.toTimeStamp(),
                    bookingEntity.end.toTimeStamp(),
                )
                .send()
        if (!transactionReceipt.isStatusOK) {
            bookingRepository.delete(bookingEntity)
            throw RuntimeException(
                "Error while adding booking ($booking) to smart contract, reverting request"
            )
        }

        val responseEntity = rentalHandlerService.updateAvailabilities(
            booking.rentalId,
            unavailability = DateRangeDto(
                booking.begin,
                booking.end
            )
        )

        if (!responseEntity.statusCode.is2xxSuccessful) {
            bookingRepository.delete(bookingEntity)
            throw RuntimeException("Error while updating availabilities")
        }
    }

    override fun isAvailable(booking: Booking): Boolean =
        !bookingRepository.existsByIdAndBeginAndEnd(
            booking.rentalId.toLong(),
            booking.begin,
            booking.end
        )

    override fun getAll(bookerUid: String): List<Booking> =
        bookingRepository.findAllByBookerUid(bookerUid).map(BookingEntity::toDomain)

    override fun get(bookingId: Int): Booking = bookingRepository.findByIdOrNull(bookingId.toLong())?.toDomain()
        ?: throw BookingNotFoundException("Booking with id $bookingId does not exist")


}