package com.ensicaen.bookinghandler.booking

import com.ensicaen.bookinghandler.api.BookingApi
import com.ensicaen.bookinghandler.authentication.getCurrentUser
import com.ensicaen.bookinghandler.extensions.toDomain
import com.ensicaen.bookinghandler.extensions.toDto
import com.ensicaen.bookinghandler.models.Booking
import com.ensicaen.openapi.api.BookersApi
import com.ensicaen.openapi.api.BookingsApi
import com.ensicaen.openapi.bookinghandler.api.BookingDto
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


private val LOGGER: Logger = LoggerFactory.getLogger(BookingController::class.java)

@RestController
@RequestMapping("/api/v1")
class BookingController(private val bookingApi: BookingApi) : BookingsApi, BookersApi {

    override fun createBooking(bookingDto: BookingDto): ResponseEntity<Unit> {
        val currentUser = getCurrentUser()
        LOGGER.info("Received request from ${currentUser.name} to create booking $bookingDto")
        bookingApi.create(bookingDto.toDomain(bookerUid = currentUser.uid))
        return ok().build()
    }

    override fun getBookings(bookerUid: String): ResponseEntity<List<BookingDto>> {
        LOGGER.info("Received request to retrieve all bookings for booker with id $bookerUid")
        return ok(bookingApi.getAll(bookerUid).map(Booking::toDto))
    }

    override fun getBooking(bookingId: Int): ResponseEntity<BookingDto> {
        LOGGER.info("Received request to retrieve booking with id $bookingId")
        return ok(bookingApi.get(bookingId).toDto())
    }
}