package com.ensicaen.bookinghandler.booking

import com.ensicaen.bookinghandler.BookingRegistryAbi
import com.ensicaen.bookinghandler.authentication.getCurrentUserToken
import com.ensicaen.bookinghandler.exceptions.BookingNotFoundException
import com.ensicaen.bookinghandler.extensions.toTimeStamp
import com.ensicaen.bookinghandler.models.Booking
import com.ensicaen.bookinghandler.rentalhandler.RentalHandlerService
import com.ensicaen.openapi.bookinghandler.api.DateRangeDto
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR
import org.springframework.http.HttpStatus.OK
import org.springframework.http.ResponseEntity
import org.web3j.protocol.core.RemoteFunctionCall
import org.web3j.protocol.core.methods.response.TransactionReceipt
import java.time.OffsetDateTime

class BookingSpiImplTest {

    private val bookingRepositoryMock: BookingRepository = mockk(relaxed = true)
    private val bookingRegistryAbiMock: BookingRegistryAbi = mockk()
    private val rentalHandlerServiceMock: RentalHandlerService = mockk()
    private val bookingSpi = BookingSpiImpl(bookingRepositoryMock, bookingRegistryAbiMock, rentalHandlerServiceMock)

    @Test
    fun create() {
        // given
        val begin = OffsetDateTime.now()
        val end = OffsetDateTime.now()
        val rentalId = 1
        val bookerUid = "bookerUid"
        val bookingId = 1
        val booking = Booking(rentalId, bookerUid, begin, end)

        val bookingEntity = BookingEntity(rentalId, bookerUid, begin, end).apply { id = bookingId.toLong() }
        every { bookingRepositoryMock.save(any()) } returns bookingEntity

        val transactionReceiptCallMock = mockk<RemoteFunctionCall<TransactionReceipt>>()
        val transactionReceiptMock = mockk<TransactionReceipt>()
        every {
            bookingRegistryAbiMock.addBooking(
                bookingId.toBigInteger(),
                rentalId.toBigInteger(),
                "bookerUid",
                begin.toTimeStamp(),
                end.toTimeStamp(),
            )
        } returns transactionReceiptCallMock
        every { transactionReceiptCallMock.send() } returns transactionReceiptMock
        every { transactionReceiptMock.isStatusOK } returns true
        mockkStatic(::getCurrentUserToken)
        every { getCurrentUserToken() } returns "token"
        val unavailability = DateRangeDto(begin, end)
        val responseEntityMock = mockk<ResponseEntity<Unit>>()
        every {
            rentalHandlerServiceMock.updateAvailabilities(
                rentalId,
                unavailability,
                "Bearer token"
            )
        } returns responseEntityMock
        every { responseEntityMock.statusCode } returns OK

        // when
        bookingSpi.create(booking)

        // then
        val bookingEntityCapture = slot<BookingEntity>()
        verifySequence {
            bookingRepositoryMock.save(capture(bookingEntityCapture))
            bookingRegistryAbiMock.addBooking(
                bookingId.toBigInteger(),
                rentalId.toBigInteger(),
                "bookerUid",
                begin.toTimeStamp(),
                end.toTimeStamp(),
            )
            transactionReceiptCallMock.send()
            transactionReceiptMock.isStatusOK
            rentalHandlerServiceMock.updateAvailabilities(rentalId, unavailability, "Bearer token")
            responseEntityMock.statusCode
        }
        val bookingEntityCaptured = bookingEntityCapture.captured
        assertThat(bookingEntityCaptured.begin).isEqualTo(begin)
        assertThat(bookingEntityCaptured.end).isEqualTo(end)
        assertThat(bookingEntityCaptured.bookerUid).isEqualTo("bookerUid")
        assertThat(bookingEntityCaptured.rentalId).isEqualTo(1)
    }

    @Test
    fun `create with error while adding booking to smart contract`() {
        // given
        val begin = OffsetDateTime.now()
        val end = OffsetDateTime.now()
        val rentalId = 1
        val bookerUid = "bookerUid"
        val bookingId = 1
        val booking = Booking(rentalId, bookerUid, begin, end)

        val bookingEntity = BookingEntity(rentalId, bookerUid, begin, end).apply { id = bookingId.toLong() }
        every { bookingRepositoryMock.save(any()) } returns bookingEntity

        val transactionReceiptCallMock = mockk<RemoteFunctionCall<TransactionReceipt>>()
        val transactionReceiptMock = mockk<TransactionReceipt>()
        every {
            bookingRegistryAbiMock.addBooking(
                bookingId.toBigInteger(),
                rentalId.toBigInteger(),
                "bookerUid",
                begin.toTimeStamp(),
                end.toTimeStamp(),
            )
        } returns transactionReceiptCallMock
        every { transactionReceiptCallMock.send() } returns transactionReceiptMock
        every { transactionReceiptMock.isStatusOK } returns false

        // when && then
        assertThatExceptionOfType(RuntimeException::class.java)
            .isThrownBy { bookingSpi.create(booking) }
            .withMessage("Error while adding booking ($booking) to smart contract, reverting request")

        val bookingEntityCapture = slot<BookingEntity>()
        verifySequence {
            bookingRepositoryMock.save(capture(bookingEntityCapture))
            bookingRegistryAbiMock.addBooking(
                bookingId.toBigInteger(),
                rentalId.toBigInteger(),
                "bookerUid",
                begin.toTimeStamp(),
                end.toTimeStamp(),
            )
            transactionReceiptCallMock.send()
            transactionReceiptMock.isStatusOK
            bookingRepositoryMock.delete(bookingEntity)
        }
        val bookingEntityCaptured = bookingEntityCapture.captured
        assertThat(bookingEntityCaptured.begin).isEqualTo(begin)
        assertThat(bookingEntityCaptured.end).isEqualTo(end)
        assertThat(bookingEntityCaptured.bookerUid).isEqualTo("bookerUid")
        assertThat(bookingEntityCaptured.rentalId).isEqualTo(1)
    }

    @Test
    fun `create with error while updating availabilities`() {
        // given
        val begin = OffsetDateTime.now()
        val end = OffsetDateTime.now()
        val rentalId = 1
        val bookerUid = "bookerUid"
        val bookingId = 1
        val booking = Booking(rentalId, bookerUid, begin, end)

        val bookingEntity = BookingEntity(rentalId, bookerUid, begin, end).apply { id = bookingId.toLong() }
        every { bookingRepositoryMock.save(any()) } returns bookingEntity

        val transactionReceiptCallMock = mockk<RemoteFunctionCall<TransactionReceipt>>()
        val transactionReceiptMock = mockk<TransactionReceipt>()
        every {
            bookingRegistryAbiMock.addBooking(
                bookingId.toBigInteger(),
                rentalId.toBigInteger(),
                "bookerUid",
                begin.toTimeStamp(),
                end.toTimeStamp(),
            )
        } returns transactionReceiptCallMock
        every { transactionReceiptCallMock.send() } returns transactionReceiptMock
        every { transactionReceiptMock.isStatusOK } returns true
        mockkStatic(::getCurrentUserToken)
        every { getCurrentUserToken() } returns "token"
        val unavailability = DateRangeDto(begin, end)
        val responseEntityMock = mockk<ResponseEntity<Unit>>()
        every {
            rentalHandlerServiceMock.updateAvailabilities(
                rentalId,
                unavailability,
                "Bearer token"
            )
        } returns responseEntityMock
        every { responseEntityMock.statusCode } returns INTERNAL_SERVER_ERROR

        // when
        assertThatExceptionOfType(RuntimeException::class.java)
            .isThrownBy { bookingSpi.create(booking) }
            .withMessage("Error while updating availabilities")

        val bookingEntityCapture = slot<BookingEntity>()
        verifySequence {
            bookingRepositoryMock.save(capture(bookingEntityCapture))
            bookingRegistryAbiMock.addBooking(
                bookingId.toBigInteger(),
                rentalId.toBigInteger(),
                "bookerUid",
                begin.toTimeStamp(),
                end.toTimeStamp(),
            )
            transactionReceiptCallMock.send()
            transactionReceiptMock.isStatusOK
            rentalHandlerServiceMock.updateAvailabilities(rentalId, unavailability, "Bearer token")
            responseEntityMock.statusCode
            bookingRepositoryMock.delete(bookingEntity)
        }
        val bookingEntityCaptured = bookingEntityCapture.captured
        assertThat(bookingEntityCaptured.begin).isEqualTo(begin)
        assertThat(bookingEntityCaptured.end).isEqualTo(end)
        assertThat(bookingEntityCaptured.bookerUid).isEqualTo("bookerUid")
        assertThat(bookingEntityCaptured.rentalId).isEqualTo(1)
    }

    @Test
    fun `isAvailable true`() {
        // given
        val rentalId = 0
        val begin = OffsetDateTime.now()
        val end = OffsetDateTime.now()
        val bookerUid = "bookerUid"
        val booking = Booking(rentalId, bookerUid, begin, end)
        every { bookingRepositoryMock.existsByIdAndBeginAndEnd(rentalId.toLong(), begin, end) } returns false

        // when
        val isAvailable = bookingSpi.isAvailable(booking)

        // then
        assertThat(isAvailable).isTrue
    }

    @Test
    fun `isAvailable false`() {
        // given
        val rentalId = 0
        val begin = OffsetDateTime.now()
        val end = OffsetDateTime.now()
        val bookerUid = "bookerUid"
        val booking = Booking(rentalId, bookerUid, begin, end)
        every { bookingRepositoryMock.existsByIdAndBeginAndEnd(rentalId.toLong(), begin, end) } returns true

        // when
        val isAvailable = bookingSpi.isAvailable(booking)

        // then
        assertThat(isAvailable).isFalse
    }

    @Test
    fun get() {
        // given
        val bookingId = 1
        val begin = OffsetDateTime.parse("2021-10-26T00:00Z")
        val end = OffsetDateTime.parse("2021-10-27T00:00Z")
        every { bookingRepositoryMock.findByIdOrNull(bookingId.toLong()) } returns BookingEntity(
            0,
            "bookerUid",
            begin,
            end,
        )

        // when
        val booking = bookingSpi.get(bookingId)

        // then
        assertThat(booking).isNotNull
        assertThat(booking.rentalId).isEqualTo(0)
        assertThat(booking.bookerUid).isEqualTo("bookerUid")
        assertThat(booking.begin).isEqualTo(OffsetDateTime.parse("2021-10-26T00:00Z"))
        assertThat(booking.end).isEqualTo(OffsetDateTime.parse("2021-10-27T00:00Z"))
    }

    @Test
    fun `get with BookingNotFoundException`() {
        // given
        val bookingId = 1
        every { bookingRepositoryMock.findByIdOrNull(bookingId.toLong()) } returns null

        // when && then
        assertThatExceptionOfType(BookingNotFoundException::class.java)
            .isThrownBy { bookingSpi.get(bookingId) }
            .withMessage("Booking with id 1 does not exist")
    }

    @Test
    fun getAll() {
        // given
        val bookerUid = "bookerUid"
        val begin = OffsetDateTime.now()
        val end = OffsetDateTime.now()
        val rentalId = 1
        every {
            bookingRepositoryMock.findAllByBookerUid(bookerUid)
        } returns listOf(BookingEntity(rentalId, bookerUid, begin, end))

        // when
        val bookings = bookingSpi.getAll(bookerUid)

        // then
        assertThat(bookings).hasSize(1)
        val expectedBooking = Booking(rentalId, bookerUid, begin, end)
        assertThat(bookings.single()).isEqualTo(expectedBooking)
    }
}