package com.ensicaen.bookinghandler.spi

import com.ensicaen.bookinghandler.models.Booking

interface BookingSpi {
    fun create(booking: Booking)
    fun getAll(bookerUid: String): List<Booking>
    fun get(bookingId: Int): Booking
    fun isAvailable(booking: Booking): Boolean
}