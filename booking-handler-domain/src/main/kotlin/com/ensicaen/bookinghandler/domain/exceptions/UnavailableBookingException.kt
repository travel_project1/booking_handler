package com.ensicaen.bookinghandler.domain.exceptions

class UnavailableBookingException(message: String) : RuntimeException(message)