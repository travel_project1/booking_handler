package com.ensicaen.bookinghandler.domain

import com.ensicaen.bookinghandler.api.BookingApi
import com.ensicaen.bookinghandler.domain.exceptions.InvalidDatesException
import com.ensicaen.bookinghandler.domain.exceptions.UnavailableBookingException
import com.ensicaen.bookinghandler.models.Booking
import com.ensicaen.bookinghandler.spi.BookingSpi
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.OffsetDateTime

private val LOGGER: Logger = LoggerFactory.getLogger(BookingApiImpl::class.java)

class BookingApiImpl(private val bookingSpi: BookingSpi) : BookingApi {

    override fun create(booking: Booking) {
        assertThatBeginIsBeforeEnd(booking.begin, booking.end)
        if (bookingSpi.isAvailable(booking)) {
            bookingSpi.create(booking)
        } else {
            val errorMessage = "Rental with id ${booking.rentalId} " +
                    "is not available between ${booking.begin} and ${booking.end}"
            LOGGER.error(errorMessage)
            throw UnavailableBookingException(errorMessage)
        }
    }

    override fun getAll(bookerUid: String): List<Booking> = bookingSpi.getAll(bookerUid)

    override fun get(bookingId: Int) = bookingSpi.get(bookingId)
}

private fun assertThatBeginIsBeforeEnd(begin: OffsetDateTime, end: OffsetDateTime) {
    if (!begin.isBefore(end)) throw InvalidDatesException("begin ($begin) must be before end ($end)")
}
