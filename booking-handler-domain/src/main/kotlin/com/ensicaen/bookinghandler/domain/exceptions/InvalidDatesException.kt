package com.ensicaen.bookinghandler.domain.exceptions

class InvalidDatesException(message: String) : RuntimeException(message)