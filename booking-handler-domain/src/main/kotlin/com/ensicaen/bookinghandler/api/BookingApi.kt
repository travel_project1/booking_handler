package com.ensicaen.bookinghandler.api

import com.ensicaen.bookinghandler.models.Booking

interface BookingApi {
    fun create(booking: Booking)
    fun getAll(bookerUid: String): List<Booking>
    fun get(bookingId: Int): Booking
}