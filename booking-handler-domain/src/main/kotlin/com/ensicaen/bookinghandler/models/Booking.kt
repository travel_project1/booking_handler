package com.ensicaen.bookinghandler.models

import java.time.OffsetDateTime

data class Booking(
    val rentalId: Int,
    val bookerUid: String,
    val begin: OffsetDateTime,
    val end: OffsetDateTime,
    val id: Int? = null,
)
