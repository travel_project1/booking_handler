package com.ensicaen.bookinghandler.domain

import com.ensicaen.bookinghandler.domain.exceptions.InvalidDatesException
import com.ensicaen.bookinghandler.domain.exceptions.UnavailableBookingException
import com.ensicaen.bookinghandler.models.Booking
import com.ensicaen.bookinghandler.spi.BookingSpi
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime
import java.time.OffsetDateTime.now

class BookingApiImplTest {

    private val bookingSpiMock: BookingSpi = mockk(relaxed = true)
    private val bookingApi = BookingApiImpl(bookingSpiMock)

    @Test
    fun create() {
        // given
        val begin = now().minusDays(1)
        val end = now()
        val booking = Booking(
            rentalId = 0,
            bookerUid = "bookerUid",
            begin = begin,
            end = end,
        )
        every { bookingSpiMock.isAvailable(booking) } returns true

        // when
        bookingApi.create(booking)

        // then
        verify(exactly = 1) { bookingSpiMock.create(booking) }
    }

    @Test
    fun `create with UnavailableBookingException`() {
        // given
        val begin = OffsetDateTime.parse("2021-10-25T00:00Z")
        val end = OffsetDateTime.parse("2021-10-26T00:00Z")
        val booking = Booking(
            rentalId = 0,
            bookerUid = "bookerUid",
            begin = begin,
            end = end,
        )
        every { bookingSpiMock.isAvailable(booking) } returns false

        // when && then
        assertThatExceptionOfType(UnavailableBookingException::class.java)
            .isThrownBy { bookingApi.create(booking) }
            .withMessage("Rental with id 0 is not available between 2021-10-25T00:00Z and 2021-10-26T00:00Z")
    }

    @Test
    fun `create with InvalidDatesException`() {
        // given
        val begin = OffsetDateTime.parse("2021-10-26T00:00Z")
        val end = OffsetDateTime.parse("2021-10-25T00:00Z")
        val booking = Booking(
            rentalId = 0,
            bookerUid = "bookerUid",
            begin = begin,
            end = end,
        )

        // when && then
        assertThatExceptionOfType(InvalidDatesException::class.java)
            .isThrownBy { bookingApi.create(booking) }
            .withMessage("begin (2021-10-26T00:00Z) must be before end (2021-10-25T00:00Z)")
    }

    @Test
    fun getAll() {
        // given
        val bookerUid = "bookerUid"
        val begin = now()
        val end = now()
        val rentalId = 1
        every {
            bookingSpiMock.getAll(bookerUid)
        } returns listOf(Booking(rentalId, bookerUid, begin, end))

        // when
        val bookings = bookingApi.getAll(bookerUid)

        // then
        assertThat(bookings).hasSize(1)
        val expectedBooking = Booking(rentalId, bookerUid, begin, end)
        assertThat(bookings.single()).isEqualTo(expectedBooking)
    }

    @Test
    fun get() {
        // given
        val begin = OffsetDateTime.parse("2021-10-26T00:00Z")
        val end = OffsetDateTime.parse("2021-10-27T00:00Z")
        val expectedBooking = Booking(
            rentalId = 0,
            bookerUid = "bookerUid",
            begin = begin,
            end = end,
        )
        val bookingId = 1

        every { bookingSpiMock.get(bookingId) } returns expectedBooking

        // when
        val booking = bookingApi.get(bookingId)

        // then
        assertThat(booking).isEqualTo(expectedBooking)
    }
}