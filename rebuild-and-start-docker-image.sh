#!/bin/sh

PROJECT_NAME="booking-handler"
CONTAINER_NAME="$PROJECT_NAME-container"
IMAGE_NAME="$PROJECT_NAME-image"

echo "Building new image ..."
sudo docker build --cache-from $IMAGE_NAME:latest -t $IMAGE_NAME:latest -f docker/Dockerfile .

echo "Starting new container ..."
sudo docker run --rm --name $CONTAINER_NAME -p 8000:8000 $IMAGE_NAME:latest

